function hasClass(elem, className) {
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

function addClass(elem, className) {
    if (!hasClass(elem, className)) {
        elem.className += ' ' + className;
    }
}

function removeClass(elem, className) {
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}

function toggleClass(elem, className) {
    const funcs = {
        false: addClass,
        true: removeClass
    }
    funcs[hasClass(elem, className)](elem, className)
}

function domSelect(selectString) {
    return document.querySelector(selectString)
}

function domSelectAll(selectString) {
    return document.querySelectorAll(selectString)
}

document.addEventListener('readystatechange', event => {
    if (event.target.readyState != 'complete') return
    domSelectAll('.project-filter li').forEach(elem => {
        elem.onclick = event => {
            const elem = event.target,
                filter = elem.getAttribute('data-filter'),
                items = domSelectAll(
                    elem.parentElement.getAttribute('data-itemselector')
                );
            domSelectAll('.project-filter li')
                .forEach(elem => removeClass(elem, 'active'))
            addClass(event.target, 'active')

            items.forEach(elem => {
                addClass(elem, 'hidden')
                elem.getAttribute('data-filter').indexOf(filter) >= 0 &&
                    removeClass(elem, 'hidden')
            })
        }
    })

    domSelectAll('.project-link').forEach(span => {
        const item = span.parentElement,
            form = span.nextElementSibling
        span.onclick = () => {
            const backdrop = document.createElement('div')
            addClass(backdrop, 'backdrop')
            backdrop.onclick = () => {
                domSelect('body').removeChild(backdrop)
                removeClass(form, 'open')
                item.appendChild(form)
            }
            backdrop.appendChild(form)
            addClass(form, 'open')
            domSelect('body').appendChild(backdrop)
        }
    })

    domSelect('.menu-trigger span').onclick = (evt => {
        toggleClass(evt.target.parentElement.parentElement, 'menu-opened')
        return true
    })
    domSelect('.menu-trigger').onclick = (evt => {
        toggleClass(evt.target.parentElement, 'menu-opened')
        console.log(evt.target)
        return true
    })
    domSelectAll('.menu-item').forEach(menu_item => {
        menu_item.onclick = (evt => {
            const menu = evt.target.parentElement.parentElement.parentElement
            removeClass(menu, 'menu-opened')
        })
        return true
    })

    const form_url = 'https://imanolvalero.pythonanywhere.com/form/contact'
    fetch(form_url)
        .then(response => response.json())
        .then(data => {
            const url = form_url + '/' + data['key']
            const form = domSelect('#contact form')
            form.addEventListener('submit', event => {
                event.preventDefault()
                const values = Array.prototype.slice.call(form.children)
                    .filter(elem => elem.type != 'submit')
                    .reduce((res, elem) => {
                        const dest = Object.assign({}, res)
                        dest[elem.name] = elem.value
                        return dest
                    }, {})
                fetch(url, {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => res.json())
                    .then(data => {
                        if (data['ok'] == true) {
                            alert('Gracias por tu mensaje, te respondere lo antes posible!')
                            Array.prototype.slice.call(form.children)
                                .filter(elem => elem.type != 'submit')
                                .forEach(elem => elem.value = '')
                        }
                    })
            })
            window.addEventListener('beforeunload', event => {
                fetch(url, {
                    method: 'DELETE'
                })
            })
            window.addEventListener('unload', event => {
                fetch(url, {
                    method: 'DELETE'
                })
            })
        })

    domSelect('.about-print').onclick = () => {
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1;
        if (isAndroid || window.print === undefined) {
            fetch('/portfolio.pdf')
                .then(res => res.blob())
                .then(blob => {
                    const anchor = document.createElement('a')
                    document.body.appendChild(anchor)
                    anchor.href = window.URL.createObjectURL(blob)
                    anchor.setAttribute('download', document.title + '.pdf')
                    anchor.click()
                    document.body.removeChild(anchor)
                });
        } else window.print()
    }
})